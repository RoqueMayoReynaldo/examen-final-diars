﻿using ExamenFinalWebApp.DB;
using ExamenFinalWebApp.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ExamenFinalWebApp.Controllers
{
    public class WelcomeController : Controller
    {
        private ExamenFinalWebAppContext context;

        public WelcomeController(ExamenFinalWebAppContext context)
        {
            this.context = context;
        }


        [HttpGet]
        public ActionResult Ingreso()
        {

            return View();
        }

        [HttpPost]
        public IActionResult Ingreso(String userName, String clave)
        {

            Usuario userN = context.Usuarios.FirstOrDefault(Usuario => Usuario.userName == userName && Usuario.clave == clave);

            if (userN == null)
            {
                ModelState.AddModelError("Mensaje", "Porfavor verifique sus credenciales");
            }


            if (ModelState.IsValid)
            {

                var claims = new List<Claim> {
                new Claim(ClaimTypes.Name,userName)
              };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                HttpContext.SignInAsync(claimsPrincipal);


                return RedirectToAction("Index", "Home");
            }


            return View();
        }




        [HttpPost]
        public IActionResult Registro(Usuario nuevoUsuario)
        {

            if (ModelState.IsValid)
            {
                context.Usuarios.Add(nuevoUsuario);
                context.SaveChanges();
                return RedirectToAction("Ingreso");

            }



            return View("Ingreso");
        }
    }
}
