﻿using ExamenFinalWebApp.DB;
using ExamenFinalWebApp.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinalWebApp.Controllers
{

    [Authorize]
    public class HomeController : Controller
    {

        private ExamenFinalWebAppContext context;

        public HomeController(ExamenFinalWebAppContext context)
        {
            this.context = context;
        }


        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ViewResult CrearCuentaForm()
        {


            return View();
        }

        [HttpPost]
        public ViewResult CrearCuenta(Cuenta cuenta)
        {
            if (ModelState.IsValid)
            {
                var claim = HttpContext.User.Claims.First();
                string username = claim.Value;

                Usuario user = context.Usuarios.FirstOrDefault(o => o.userName == username);

                cuenta.idUsuario = user.id;


                context.Cuentas.Add(cuenta);
                context.SaveChanges();

                return View("Ok");


            }




            return View("CrearCuentaForm");
        }



        [HttpGet]
        public ViewResult CrearGastoForm()
        {


            return View();
        }



        [HttpPost]
        public ViewResult CrearGasto(String nombreCuenta, Gasto gasto)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;

            Usuario user = context.Usuarios.FirstOrDefault(o => o.userName == username);

            if (string.IsNullOrEmpty(nombreCuenta))
            {
                ModelState.AddModelError("nombreCuenta","Cuenta requerida");
            }
            else
            {
                Cuenta cuenta = context.Cuentas.FirstOrDefault(o => o.idUsuario == user.id && o.nombre == nombreCuenta);

                if (cuenta==null)
                {
                    ModelState.AddModelError("nombreCuenta", "Cuenta no existente para el usuario");
                }else if (gasto.monto>cuenta.saldo)
                {

                    ModelState.AddModelError("monto", "Monto excede al saldo disponible");

                }

            }

         

            if (ModelState.IsValid)
            {
                Cuenta c = context.Cuentas.FirstOrDefault(o => o.idUsuario == user.id && o.nombre == nombreCuenta);

                gasto.idCuenta = c.id;

                c.saldo -= gasto.monto;

                context.SaveChanges();


                context.Gastos.Add(gasto);
                context.SaveChanges();

                return View("Ok");

            }


            return View("CrearGastoForm");
        }
        

        [HttpGet]
        public ViewResult ObtenerGastos()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;

            Usuario user = context.Usuarios.Include("cuentas.gastos").FirstOrDefault(o => o.userName == username);

            

            return View(user);
        }









        [HttpGet]
        public ViewResult CrearIngresoForm()
        {


            return View();
        }




        [HttpPost]
        public ViewResult CrearIngreso(String nombreCuenta, Ingreso ingreso)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;

            Usuario user = context.Usuarios.FirstOrDefault(o => o.userName == username);

            if (string.IsNullOrEmpty(nombreCuenta))
            {
                ModelState.AddModelError("nombreCuenta", "Cuenta requerida");
            }
            else
            {
                Cuenta cuenta = context.Cuentas.FirstOrDefault(o => o.idUsuario == user.id && o.nombre == nombreCuenta);

                if (cuenta == null)
                {
                    ModelState.AddModelError("nombreCuenta", "Cuenta no existente para el usuario");
                }
              

            }



            if (ModelState.IsValid)
            {
                Cuenta c = context.Cuentas.FirstOrDefault(o => o.idUsuario == user.id && o.nombre == nombreCuenta);

                ingreso.idCuenta = c.id;

                c.saldo += ingreso.monto;

                context.SaveChanges();


                context.Ingresos.Add(ingreso);
                context.SaveChanges();

                return View("Ok");

            }


            return View("CrearIngresoForm");
        }


        [HttpGet]
        public ViewResult ObtenerIngresos()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;

            Usuario user = context.Usuarios.Include("cuentas.ingresos").FirstOrDefault(o => o.userName == username);



            return View(user);
        }



        [HttpGet]
        public ViewResult CrearTransferenciaForm()
        {



            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;

            Usuario user = context.Usuarios.Include("cuentas").FirstOrDefault(o => o.userName == username);



            return View(user);
        }


        [HttpPost]
        public ViewResult CrearTransferencia(int cuentaOrigen,int cuentaDestino,DateTime fecha,Double monto)
        {



            Cuenta cuentaO = context.Cuentas.FirstOrDefault(o => o.id == cuentaOrigen);
            Cuenta cuentaD = context.Cuentas.FirstOrDefault(o => o.id == cuentaDestino);









            if (monto < 0)
            {
                ModelState.AddModelError("montoT", "El monto no puede ser negativo");


            }

            if (monto > cuentaO.saldo)
            {

                ModelState.AddModelError("montoT", "Monto excede al saldo disponible");

            }



            if (ModelState.IsValid)
            {

                Gasto gasto = new Gasto();
                gasto.idCuenta = cuentaO.id;
                gasto.fecha = fecha;
                gasto.monto = monto;
                gasto.descripcion = "Transferencia";

                cuentaO.saldo -= gasto.monto;
                context.SaveChanges();

                context.Gastos.Add(gasto);
                context.SaveChanges();




                //Ingreso

                Ingreso ingreso = new Ingreso();
                ingreso.idCuenta = cuentaD.id;
                ingreso.fecha = fecha;
                ingreso.monto = monto;
                ingreso.descripcion = "Transferencia";



                cuentaD.saldo += ingreso.monto;
                context.SaveChanges();

                context.Ingresos.Add(ingreso);
                context.SaveChanges();


            }


            return View("OK");
        }



        [HttpGet]
        public ViewResult obtenerSaldosTotales()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;

            Usuario user = context.Usuarios.FirstOrDefault(o => o.userName == username);

            List<Cuenta> cuentasCredito = context.Cuentas.Include(o=>o.gastos).Include(o => o.ingresos).Where(o=>o.categoria=="Credito" && o.idUsuario==user.id ).ToList();

            double gastosCredito=0;
            double donacionesToCreditos=0;
            double saldoCreditos = 0;


            foreach (Cuenta cuenta in cuentasCredito)
            {
                saldoCreditos += cuenta.saldo;
                foreach (Gasto gasto in cuenta.gastos)
                {
                    gastosCredito += gasto.monto;
                }

                foreach (Ingreso ingreso in cuenta.ingresos)
                {
                    donacionesToCreditos += ingreso.monto;
                }

            }


            List<Cuenta> cuentasPropio = context.Cuentas.Include(o => o.gastos).Include(o => o.ingresos).Where(o => o.categoria == "Propio" && o.idUsuario == user.id).ToList();

            double saldoPropios=0;
            foreach (Cuenta cuenta in cuentasPropio)
            {
                saldoPropios += cuenta.saldo;

            }


            double cuantoTengo = saldoPropios - gastosCredito;
            cuantoTengo+= donacionesToCreditos;

            double cuantoPuedoGastar = saldoCreditos;
            if (cuantoTengo >= 0)
            {
                cuantoPuedoGastar = saldoCreditos + cuantoTengo;

            }

            ViewBag.Tengo= cuantoTengo;
            ViewBag.Puedo= cuantoPuedoGastar;






            return View();
        }




        [HttpGet]

        public IActionResult Aceptar(int id)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;


            Usuario user = context.Usuarios.FirstOrDefault(o => o.userName == username);


            Solicitud solicitude = context.Solicituds.FirstOrDefault(o => o.idRemitente == id);
            context.Solicituds.Remove(solicitude);
            context.SaveChanges();


            Amigo amigo = new Amigo();

            amigo.idUsuario = user.id;
            amigo.idAmigo = id;

            context.Amigos.Add(amigo);
            context.SaveChanges();



            return View("Index");
        }





     





        [HttpGet]

        public IActionResult Solicitudes()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;


            Usuario user = context.Usuarios.FirstOrDefault(o => o.userName == username);


            List<Solicitud> solicitudes = context.Solicituds.Where(o => o.idReceptor == user.id).ToList();

            List<Usuario> usuarios=new List<Usuario>();

            foreach (Solicitud sol in solicitudes)
            {
                Usuario usua = context.Usuarios.FirstOrDefault(o => o.id == sol.idRemitente);
                usuarios.Add(usua);

            }



            return View(usuarios);
        }







        [HttpPost]
        public ViewResult search(string buscar)
        {

            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;

            Usuario user = context.Usuarios.FirstOrDefault(o => o.userName == username);

            if (string.IsNullOrEmpty(buscar))
            {
                buscar = " ";
            }

            List<Usuario> usuarios = context.Usuarios.Where(o => o.nombre.Contains(buscar)).ToList();

            List<Amigo> amigos = context.Amigos.Where(o=>o.idUsuario==user.id).ToList();

            List<Solicitud> solicitudes = context.Solicituds.Where(o => o.idRemitente == user.id).ToList();

            ViewBag.amigos = amigos;

            ViewBag.solicituds = solicitudes;
            return View(usuarios);

        }



        [HttpGet]

        public IActionResult Agregar(int id)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;

            Usuario user = context.Usuarios.FirstOrDefault(o => o.userName == username);
            Solicitud solicitud = new Solicitud();

            solicitud.idReceptor = id;
            solicitud.idRemitente = user.id;

            context.Solicituds.Add(solicitud);
            context.SaveChanges();



           

            return RedirectToAction("Index", "Home");
        }







        [HttpGet]

        public IActionResult Logout()
        {




            HttpContext.SignOutAsync();

            return RedirectToAction("Ingreso", "Welcome");
        }


    }
}
