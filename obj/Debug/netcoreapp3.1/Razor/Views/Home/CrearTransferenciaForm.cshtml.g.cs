#pragma checksum "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e163c37b5a251ec7c03bb3a52f8a60fded7216c2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_CrearTransferenciaForm), @"mvc.1.0.view", @"/Views/Home/CrearTransferenciaForm.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\_ViewImports.cshtml"
using ExamenFinalWebApp;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\_ViewImports.cshtml"
using ExamenFinalWebApp.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e163c37b5a251ec7c03bb3a52f8a60fded7216c2", @"/Views/Home/CrearTransferenciaForm.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ebb1254584e1d34f3d00fba1d180bd96b7e95e18", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_CrearTransferenciaForm : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
  
    Layout = null;

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<div style=""background-color:burlywood; border:double"" ;>

    <div class=""d-flex"" style=""justify-content: space-between;"">
        <h3>Generar Transferencia</h3>
        <div>
            <button id=""createTransferencia"" class=""btn btn-success"">Crear</button>

            <button id=""cancelCreateTransferencia"" class=""btn btn-danger"">Cancelar</button>

        </div>


    </div>

    <div class=""form-group"">
        <label for=""inpCuentaOrigen"">Cuenta Origen</label>
        <select name=""cuentaOrigen"" class=""form-control"" id=""inpCuentaOrigen"">

");
#nullable restore
#line 23 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
             foreach (Cuenta cuenta in Model.cuentas)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("               ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e163c37b5a251ec7c03bb3a52f8a60fded7216c24232", async() => {
#nullable restore
#line 25 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
                                   Write(cuenta.nombre);

#line default
#line hidden
#nullable disable
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 25 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
                 WriteLiteral(cuenta.id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
#nullable restore
#line 26 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n\r\n        </select>\r\n        <div><span class=\"text-danger\"> ");
#nullable restore
#line 31 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
                                   Write(Html.ValidationMessage("cuentaOrigen"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</span></div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <label for=\"inpCuentaDestino\">Cuenta Destino</label>\r\n        <select name=\"cuentaDestino\" class=\"form-control\" id=\"inpCuentaDestino\">\r\n\r\n");
#nullable restore
#line 38 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
             foreach (Cuenta cuenta in Model.cuentas)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e163c37b5a251ec7c03bb3a52f8a60fded7216c27108", async() => {
#nullable restore
#line 40 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
                                    Write(cuenta.nombre);

#line default
#line hidden
#nullable disable
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 40 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
                  WriteLiteral(cuenta.id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
#nullable restore
#line 41 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n\r\n        </select>\r\n        <div><span class=\"text-danger\"> ");
#nullable restore
#line 46 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
                                   Write(Html.ValidationMessage("cuentaDestino"));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</span></div>
    </div>



    <div class=""form-group"">
        <label for=""datetimeT"">Fecha y Hora</label>

        <input class=""form-control"" type=""datetime-local"" id=""datetimeT""
               name=""fecha"" value=""2021-07-09T19:30""
               min=""2019-01-01T00:00"">


        <div><span class=""text-danger""> ");
#nullable restore
#line 59 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
                                   Write(Html.ValidationMessage("fechaT"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</span></div>\r\n    </div>\r\n\r\n\r\n    <div class=\"form-group\">\r\n        <label for=\"inpMontoT\">Monto</label>\r\n        <input name=\"montoT\" type=\"text\" class=\"form-control\" id=\"inpMontoT\" placeholder=\"S/\">\r\n        <div><span class=\"text-danger\"> ");
#nullable restore
#line 66 "C:\Users\FUCK\source\repos\ExamenFinalWebApp\Views\Home\CrearTransferenciaForm.cshtml"
                                   Write(Html.ValidationMessage("montoT"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</span></div>\r\n    </div>\r\n\r\n\r\n\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
