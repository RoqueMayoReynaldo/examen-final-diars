﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinalWebApp.Models
{
    public class Usuario
    {
        public int id { get; set; }

        [Required(ErrorMessage = "Nombre es requerido")]
        public String nombre { get; set; }

        [Required(ErrorMessage = "DNI es requerido")]
        [RegularExpression("([1-9][1-9][1-9][1-9][1-9][1-9][1-9][1-9])", ErrorMessage = "8 digitos dni")]
        public int dni { get; set; }

        [Required(ErrorMessage = "Telefono es requerido")]
        public int telefono { get; set; }

        [EmailAddress(ErrorMessage ="Formato Incorrecto")]
        [Required(ErrorMessage = "Correo es requerido")]
        public String correo { get; set; }

        [Required(ErrorMessage = "Usuario es requerido")]
        [MinLength(5, ErrorMessage = "Se requiere almenos 5 caracteres")]

        public String userName { get; set; }

        [Required(ErrorMessage = "Clave es requerida")]
        [MinLength(5, ErrorMessage = "Se requiere almenos 5 caracteres")]
        public String clave { get; set; }

        public List<Cuenta> cuentas { get; set; }


    }
}
