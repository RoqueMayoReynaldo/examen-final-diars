﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinalWebApp.Models
{
    public class Gasto
    {

        public int id { get; set; }
        public int idCuenta { get; set; }

        [Required(ErrorMessage = "Fecha requerida")]
        public DateTime fecha { get; set; }
        [Required(ErrorMessage = "Descripcion requerida")]
        public String descripcion { get; set; }
        [Required(ErrorMessage = "Monto requerido")]
        [Range(0, 99999999999)]
        public double monto { get; set; }


    }
}
