﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinalWebApp.Models
{
    public class Cuenta
    {
        public int id { get; set; }
        public int idUsuario { get; set; }
        [Required(ErrorMessage ="Nombre requerido")]
        public String nombre { get; set; }
        [Required(ErrorMessage = "Categoria requerida")]
        public String categoria { get; set; }
        [Required(ErrorMessage = "Salgo inicial requerido")]
        public double saldo { get; set; }

        public List<Ingreso> ingresos { get; set; }
        public List<Gasto> gastos { get; set; }


    }
}
