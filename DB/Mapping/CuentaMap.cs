﻿using ExamenFinalWebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinalWebApp.DB.Mapping
{

    public class CuentaMap : IEntityTypeConfiguration<Cuenta>

    {
        public void Configure(EntityTypeBuilder<Cuenta> builder)
        {


            builder.ToTable("Cuenta", "dbo");
            builder.HasKey(Cuenta => Cuenta.id);

            builder.HasMany(Cuenta => Cuenta.ingresos).WithOne().HasForeignKey(o => o.idCuenta);
            builder.HasMany(Cuenta => Cuenta.gastos).WithOne().HasForeignKey(o => o.idCuenta);

        }
    }

}
