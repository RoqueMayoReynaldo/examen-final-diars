﻿using ExamenFinalWebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinalWebApp.DB.Mapping
{

    public class IngresoMap : IEntityTypeConfiguration<Ingreso>

    {
        public void Configure(EntityTypeBuilder<Ingreso> builder)
        {


            builder.ToTable("Ingreso", "dbo");
            builder.HasKey(Ingreso => Ingreso.id);

        

        }
    }

}